
var badContentXpathes = [
    '//div[contains(translate(@id, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),  "comments")]',
    '//section[starts-with(translate(@id, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdeffghijklmnopqrstuvwxyz"),  "service-comments")]',
    '//div[starts-with(translate(@class, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdeffghijklmnopqrstuvwxyz"),  "mask-left")]',  
    '//div[starts-with(translate(@class, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),  "adthrive")]',
    '//div[starts-with(translate(@class, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),  "atkp-box")]',
    '//div[starts-with(translate(@class, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdeffghijklmnopqrstuvwxyz"),  "google-auto-")]',
    '//div[starts-with(translate(@class, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdeffghijklmnopqrstuvwxyz"),  "rnet-widget")]',
    '//div[starts-with(translate(@class, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdeffghijklmnopqrstuvwxyz"),  "sidebar")]',   
    '//div[contains(translate(@class, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),  "video-wrapper")]',
    '//ins[contains(translate(@class, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),  "adsbygoogle")]',
    '//p[contains(.,"clicking the button")]',
    '//div/a[contains(.," FREE ")]',
    '(//a|//button)[contains(translate(@title, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),  "share on facebook")]',
    '(//a|//button)[contains(translate(@title, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),  "save to pinterest")]',
    '//div[@class="mv-ad-box"]',
    '//div[@class="mv-ad-box"]',
    '//p[contains(.,"affiliate links")]',
    '(//p)[contains(translate(., "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),  "learn more here")]',
    '(//a|//button)[contains(translate(., "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),  "price")]',
    '(//a|//button)[contains(translate(., "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),  "buy")]',
    '(//p)[contains(translate(., "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),"check out") and a[@href]]',
    '(//p)[contains(translate(., "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz")," amazon ")]',
    '//ul[contains(./li//a/@href,"amazon.com")]',
    '//ul[contains(./li//a/@href,"amzn.to")]',
    '(//strong)[contains(translate(., "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz")," useful items for ")]',
    '//iframe',
    '//input',
    '//noscript',
    '//script',
    '//javascript',
    '//figcaption',
    '//style',
    '//svg',
    `//aside`,
    '//nav',
    '//span[contains(.,"photo courtesy of")]',
    '//p[contains(.,"Learn More:")   and .//a[@href]   ]',
    '//p[contains(./text(),"Featured:")   and .//a[@href]   ]',
    '//a[contains(translate(., "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),"source")]',
    '//p[contains(.,"Subscribe")  and (contains(.//a/@href, "itunes" )    or  //p[contains(.,"Subscribe")  and       contains(translate(.//a/text(), "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"), "spotify" )        ]       ) ]',
    '//p[contains(translate(., "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),"listen")  and   contains(translate(., "ABCDEFGHIJKLMNOPQRSTUVWXYZ", "abcdefghijklmnopqrstuvwxyz"),"podcast")    ]',
     //new
    '//*[@id="toc_container"]',
    '//div[contains(./div/a/text(),"View on Amazon")]',
    '//button',
    ]